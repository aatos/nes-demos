;;; Small helloworld demo application for NES
    .inesprg 1                  ; One PRG bank (16 KB)
    .ineschr 1                  ; One CHR bank (8 KB)
    .inesmap 0                  ; No mapper
    .inesmir 0                  ; No background mirroring (Whatever that is)

    .bank 0
    .org $C000                  ; ROM starts from 0x8000 but leave some room for e.g. variables
spriteX = 0
spriteY = 1

init:
    sei                         ; Disable interrupts (since we don't handle them)
    cld                         ; Disable decimal mode (since NES doesn't support it)
    ;; Initialize stack
    ldx #$ff
    txs
    ldx #$0
    stx $2000                   ; Disable NMI
    stx $2001                   ; Disable rendering
    stx $4010                   ; Disable DMC IRQ
;;; Need to wait for two vblanks
vblankWait1:
    bit $2002
    bpl vblankWait1
;;; Initialize memory now since second vblank takes time
    lda #$0
initMem:
    sta $0000,x
    sta $0100,x
    ;; Skip over $0200 since it is reserved for OAM
    ;; TODO: Should it be inited now since we are using $0200?
    sta $0300,x
    sta $0400,x
    sta $0500,x
    sta $0600,x
    sta $0700,x
    inx
    bne initMem
vblankWait2:
    bit $2002
    bpl vblankWait2

setPaletteAddress:
    lda $2002                   ; Read PPU status which will reset PPU to expect high byte next
    ;; Load palettes to $3f00
    lda #$3f
    sta $2006
    lda #$00
    sta $2006
    ldx #$00
loadPalettes:
    lda PaletteData, x
    sta $2007
    inx
    cpx #$20
    bne loadPalettes

initPPU:
    ;; TODO: can this be done before first vblank?
    lda #%10000000              ; Enable NMI and sprites from pattern table 0
    sta $2000
    lda #%00010000              ; Enable rendering and sprites
    sta $2001
initVars:
    ldx #$80
    stx spriteX
    ldx #$78
    stx spriteY

;;; Load sprites to the address that is used in DMA
loadSprites:
    ldx #$00
loadSpritesLoop:
    lda SpriteData, x
    sta $0200, x
    inx
    cpx #24
    bne loadSpritesLoop

main:
    ldx #$00
mainSkip:
    lda SpriteData, x
    clc
    adc spriteY
    sta $0200, x
    ;; TODO: Bit ugly
    inx
    inx
    inx
    lda SpriteData, x
    clc
    adc spriteX
    sta $0200, x
    inx
    cpx #24
    bne mainSkip
    jmp main

nmi:
    ;; Transfer sprite data from $0200 using DMA
    ;; Set OAMData address to zero by writing to $2003
    lda #$00
    sta $2003
    ;; Start DMA from $0200 by writing $02 to $4014
    lda #$02
    sta $4014
    inc spriteX
latchContoller:
    lda #$1
    sta $4016
    lda #$0
    sta $4016
readA:
    lda $4016
readB:
    lda $4016
readSelect:
    lda $4016
readStart:
    lda $4016
readUp:
    lda $4016
    and #%00000001
    beq readDown
    dec spriteY
readDown:
    lda $4016
    and #%00000001
    beq readLeft
    inc spriteY
readLeft:
    lda $4016
    and #%00000001
    beq readRight
    dec spriteX
readRight:
    lda $4016
    and #%00000001
    beq done
    inc spriteX
done:
    rti

    ;; Setup vectors
    .bank 1
    .org $f000
PaletteData:
    .db $0f,$31,$32,$33,$0f,$35,$36,$37,$0f,$39,$3a,$3b,$0f,$3d,$3e,$0f
    .db $0f,$2a,$15,$14,$0f,$02,$38,$3c,$0f,$1c,$15,$14,$0f,$02,$38,$3c
SpriteData:
    ;; Y, #Tile, Attrs, X
    .db $00,$48,$00,0
    .db $00,$45,$00,7
    .db $00,$48,$00,14
    .db $00,$48,$00,28
    .db $00,$45,$00,35
    .db $00,$48,$00,42

    .org $fffa
    .dw nmi                     ; Handler for NMI (VBlank is starting)
    .dw init                    ; Handler for reset
    .dw 0

    .bank 2
    .org $0000
    .incbin "asciipong.chr"
