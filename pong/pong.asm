;;; Simple pong game for NES
;;;
;;; Few TODOS:
;;; - Game only counts points with one digit. Second one is currently not used
;;; and after 9, the game will start to use alphabets.
;;; - Ball can somehow go to the scoreboard and won't come down.
;;; - Hitboxes are a bit off.
;;; - There is quite much code duplication, e.g. when handling both player inputs

    .inesprg 1                  ; One PRG bank (16 KB)
    .ineschr 1                  ; One CHR bank (8 KB)
    .inesmap 0                  ; No mapper
    .inesmir 0                  ; No background mirroring (TODO: Whatever that is)

    .rsset $0000
ballDirX    .rs 1
ballDirY    .rs 1

    .bank 0
    .org $C000                  ; ROM starts from 0x8000 but leave some room for e.g. variables

RIGHT_WALL = 250
LEFT_WALL = 0
TOP_WALL = 46
BOTTOM_WALL = 220
PLAYER_1_Y = $200
PLAYER_1_X = $203
PLAYER_1_SCORE = $219
PLAYER_2_SCORE = $221
PLAYER_2_Y = $208
PLAYER_2_X = $20B
BALL_Y = $210
BALL_X = $213
BALL_START_Y = 120
BALL_START_X = 128


init:
    sei                         ; Disable interrupts (since we don't handle them)
    cld                         ; Disable decimal mode (since NES doesn't support it)
    ;; Initialize stack
    ldx #$ff
    txs
    ldx #$0
    stx $2000                   ; Disable NMI
    stx $2001                   ; Disable rendering
    stx $4010                   ; Disable DMC IRQ
;;; Need to wait for two vblanks
vblankWait1:
    bit $2002
    bpl vblankWait1
;;; Initialize memory now since second vblank takes time
    lda #$0
initMem:
    sta $0000,x
    sta $0100,x
    ;; Skip over $0200 since it is reserved for OAM
    ;; TODO: Should it be inited now since we are using $0200?
    sta $0300,x
    sta $0400,x
    sta $0500,x
    sta $0600,x
    sta $0700,x
    inx
    bne initMem
vblankWait2:
    bit $2002
    bpl vblankWait2

setPaletteAddress:
    lda $2002                   ; Read PPU status which will reset PPU to expect high byte next
    ;; Load palettes to $3f00
    lda #$3f
    sta $2006
    lda #$00
    sta $2006
    ldx #$00
loadPalettes:
    lda PaletteData, x
    sta $2007
    inx
    cpx #$20
    bne loadPalettes
setNametableAddress:
    lda $2002
    ;; Load Background to $2000
    lda #$20
    sta $2006
    lda #$00
    sta $2006
    ldx #$00
loadNametable:
    lda Nametable, x
    sta $2007
    inx
    cpx #$80
    bne loadNametable
setAttributeTableAddress:
    lda $2002
    ;; Load Background to $23C0
    lda #$23
    sta $2006
    lda #$C0
    sta $2006
    ldx #$00
loadAttributeTable:
    lda AttributeTable, x
    sta $2007
    inx
    cpx #$08
    bne loadAttributeTable

initPPU:
    ;; TODO: can this be done before first vblank?
    lda #%10000000              ; Enable NMI and sprites (and BG) from pattern table 0
    sta $2000
    lda #%00011110              ; Enable rendering and sprites
    sta $2001
initVars:
    lda #$00
    sta ballDirX
    sta ballDirY
;;; Load sprites to the address that is used in DMA
loadSprites:
    ldx #$00
loadSpritesLoop:
    lda SpriteData, x
    sta $0200, x
    inx
    cpx #36
    bne loadSpritesLoop

main:
    jmp main

handleControllers:
    ;; TODO: this could be prettier
    lda #$1
    sta $4016
    lda #$0
    sta $4016
read1A:
    lda $4016
read1B:
    lda $4016
read1Select:
    lda $4016
read1Start:
    lda $4016
read1Up:
    lda $4016
    and #%00000001
    beq read1Down
    lda PLAYER_1_Y
    cmp #TOP_WALL
    beq read1Down
    dec PLAYER_1_Y
    dec $0204
read1Down:
    lda $4016
    and #%00000001
    beq read1Left
    lda PLAYER_1_Y
    cmp #BOTTOM_WALL
    beq read1Left
    inc PLAYER_1_Y
    inc $0204
read1Left:
    lda $4016
    and #%00000001
    beq read1Right
read1Right:
    lda $4016
read2A:
    lda $4017
read2B:
    lda $4017
read2Select:
    lda $4017
read2Start:
    lda $4017
read2Up:
    lda $4017
    and #%00000001
    beq read2Down
    lda PLAYER_2_Y
    cmp #TOP_WALL
    beq read2Down
    dec PLAYER_2_Y
    dec $020C
read2Down:
    lda $4017
    and #%00000001
    beq read2Left
    lda PLAYER_2_Y
    cmp #BOTTOM_WALL
    beq read2Left
    inc PLAYER_2_Y
    inc $020C
read2Left:
    lda $4017
    and #%00000001
    beq read2Right
read2Right:
    lda $4017
    rts

handleBall:
    ;; Reset ball to center if it touches left or right wall
    lda BALL_X
    cmp #RIGHT_WALL
    bne checkLeftWall
    lda #BALL_START_X
    sta BALL_X
    inc PLAYER_1_SCORE
    jmp moveBallX
checkLeftWall:
    cmp #LEFT_WALL
    bne checkTopWall
    lda #BALL_START_X
    sta BALL_X
    inc PLAYER_2_SCORE
    jmp moveBallX
checkTopWall:
    ;; Change ball's y direction  if it touches bottom or top wall
    lda BALL_Y
    cmp #TOP_WALL
    bne checkBottomWall
    lda ballDirY
    eor #$01
    sta ballDirY
    jmp moveBallX
checkBottomWall:
    cmp #BOTTOM_WALL
    bne handlePlayer1
    lda ballDirY
    eor #$01
    sta ballDirY
    jmp moveBallX
handlePlayer1:
    lda PLAYER_1_Y
    ldx #$00
player1HitLoop:
    clc
    ;; TODO: fix the hitbox
    ;; adc #$02                    ; To make ball centered
    cmp BALL_Y
    bne noHit1
    lda PLAYER_1_X
    clc
    adc #$04
    cmp BALL_X
    bne noHit1
player1Hit:
    lda ballDirX
    eor #$01
    sta ballDirX
    jmp moveBallX
noHit1:
    inx
    txa
    clc
    adc PLAYER_1_Y
    cpx #$12
    bne player1HitLoop
handlePlayer2:
    lda PLAYER_2_Y
    ldx #$00
player2HitLoop:
    clc
    ;; TODO: fix the hitbox
    ;; adc #$02                    ; To make ball centered
    cmp BALL_Y
    bne noHit2
    lda PLAYER_2_X
    sec
    sbc #$04
    cmp BALL_X
    bne noHit2
player2Hit:
    lda ballDirX
    eor #$01
    sta ballDirX
    jmp moveBallX
noHit2:
    inx
    txa
    clc
    adc PLAYER_2_Y
    cpx #$12
    bne player2HitLoop
moveBallX:
    lda ballDirX
    cmp #$01
    bne moveBackward
    lda ballDirX
    inc BALL_X
    lda #$00
    cmp #$00
    beq moveBallY
moveBackward:
    lda ballDirX
    dec BALL_X
moveBallY:
    lda ballDirY
    cmp #$01
    bne moveDownward
    lda ballDirY
    inc BALL_Y
    lda #$00
    cmp #$00
    beq return
moveDownward:
    lda ballDirY
    dec BALL_Y
return:
    rts

nmi:
    ;;; Transfer sprite data from $0200 using DMA
    ;; Set OAMData address to zero by writing to $2003
    lda #$00
    sta $2003
    ;; Start DMA from $0200 by writing $02 to $4014
    lda #$02
    sta $4014
    jsr handleControllers
    jsr handleBall
    rti

    .bank 1
    .org $e000
PaletteData:
    .db $0f,$02,$22,$33,$0f,$35,$36,$37,$0f,$39,$3a,$3b,$0f,$3d,$3e,$0f
    .db $0f,$2a,$15,$14,$0f,$02,$38,$3c,$0f,$1c,$15,$14,$0f,$02,$38,$3c
SpriteData:
    ;; Y, #Tile, Attrs, X
    .db 128,$01,$01,10
    .db 135,$01,$01,10
    .db 128,$02,$02,240
    .db 135,$02,$02,240
    .db BALL_START_Y,$03,$00,BALL_START_X
    .db 16,$30,$00,16
    .db 16,$30,$00,24
    .db 16,$30,$00,222
    .db 16,$30,$00,230
Nametable:
    .db $07,$08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$08
    .db $08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$09

    .db $17,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    .db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$19

    .db $17,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    .db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$19

    .db $27,$28,$28,$28,$28,$28,$28,$28,$28,$28,$28,$28,$28,$28,$28,$28
    .db $28,$28,$28,$28,$28,$28,$28,$28,$28,$28,$28,$28,$28,$28,$28,$29
AttributeTable:
    .db %00000000, %00000000, %00000000, %00000000, %00000000, %00000000, %00000000, %00000000

    .org $fffa
    .dw nmi                     ; Handler for NMI (VBlank is starting)
    .dw init                    ; Handler for reset
    .dw 0

    .bank 2
    .org $0000
    .incbin "asciipong.chr"
