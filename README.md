# NES Demos

Simple nes demos for learning 6502 assembly. Mostly followed
http://nintendoage.com/forum/messageview.cfm?catid=22&threadid=7155.

Sprites are done with [asesprite](http://www.aseprite.org/) and the original can
be found in `asciipong.ase`. It is exported to `asciipong.bmp`, so `asesprite`
is not required for building.

## simpler-helloworld

Static sprites drawn to the screen. No NMI, no input (=nothing complex to emulate).

![simpler-helloworld](https://i.imgur.com/wXgCK9t.jpg)

## helloworld

Text scrolling across the screen. Movable with directional buttons.

![helloworld](https://i.imgur.com/Sj2c8QY.gif)

## pong

Pong game for two players. Supports two controllers. Move with up and down
buttons.

![pong](https://i.imgur.com/DBIoTv9.gif)

## Building

[bmp2chr](http://bobrost.com/nes/files/nes_sprite_tools_2004_03_18.zip) is
required, which translates the sprite bitmap to a format that is understood by
the NES. Some nes assembler is required also. I used
[nesasm3](http://www.nespowerpak.com/nesasm/nesasmsrc.zip). Both `bmp2chr` and
`nesasm3` seem to come with quite permissive licenses.

### To build all demos

``` sh
$ make
```
