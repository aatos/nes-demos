# NES Demos

**Moved to https://gitlab.com/aatos/nes-demos**

Simple nes demos for learning 6502 assembly. Mostly followed
http://nintendoage.com/forum/messageview.cfm?catid=22&threadid=7155.

## simpler-helloworld

Static sprites drawn to the screen. No NMI, no input (=nothing complex to emulate).

![simpler-helloworld](http://i.imgur.com/gxxF1Hf.png)

## helloworld

Text scrolling across the screen. Movable with directional buttons.

![helloworld](http://i.imgur.com/pftSZgW.gif)

## pong

Pong game for two players. Supports two controllers. Move with up and down
buttons.

![pong](http://i.imgur.com/Wutz34W.gif)
