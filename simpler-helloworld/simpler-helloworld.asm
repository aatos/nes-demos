;;; Simpler (=no input or NMI or DMA) helloworld demo application for NES
;;; Displays letter 'H' at the center of the screen

    .inesprg 1                  ; One PRG bank (16 KB)
    .ineschr 1                  ; One CHR bank (8 KB)
    .inesmap 0                  ; No mapper
    .inesmir 0                  ; No background mirroring (Whatever that is)

    .bank 0
    .org $C000                  ; ROM starts from 0x8000 but leave some room for e.g. variables

init:
    sei                         ; Disable interrupts (since we don't handle them)
    cld                         ; Disable decimal mode (since NES doesn't support it)
    ;; Initialize stack
    ldx #$ff
    txs
    ldx #$0
    stx $2000                   ; Disable NMI
    stx $2001                   ; Disable rendering
    stx $4010                   ; Disable DMC IRQ
;;; Need to wait for two vblanks
vblankWait1:
    bit $2002
    bpl vblankWait1
;;; Initialize memory now since second vblank takes time
    lda #$0
initMem:
    sta $0000,x
    sta $0100,x
    ;; Skip over $0200 since it is reserved for OAM
    ;; TODO: Should it be inited now since we are using $0200?
    sta $0300,x
    sta $0400,x
    sta $0500,x
    sta $0600,x
    sta $0700,x
    inx
    bne initMem
vblankWait2:
    bit $2002
    bpl vblankWait2

setPaletteAddress:
    lda $2002                   ; Read PPU status which will reset PPU to expect high byte next
    ;; Load palettes to $3f00
    lda #$3f
    sta $2006
    lda #$00
    sta $2006
    ldx #$00
loadPalettes:
    lda PaletteData, x
    sta $2007
    inx
    cpx #$20
    bne loadPalettes

initPPU:
    ;; TODO: can this be done before first vblank?
    lda #%10000000              ; Enable NMI and sprites from pattern table 0
    sta $2000
    lda #%00010000              ; Enable rendering and sprites
    sta $2001
;;; Load a sprite directly to PPU
loadSprites:
    lda #$00
    sta $2003
	ldx #$00
loadSpritesLoop:
    lda SpriteData, x
    sta $2004
    inx
    cpx #40
    bne loadSpritesLoop

main:
    jmp main

nmi:
    rti

    ;; Setup vectors
    .bank 1
    .org $f000
PaletteData:
    .db $0f,$31,$32,$33,$0f,$35,$36,$37,$0f,$39,$3a,$3b,$0f,$3d,$3e,$0f
    .db $0f,$2a,$15,$14,$0f,$02,$38,$3c,$0f,$1c,$15,$14,$0f,$02,$38,$3c
SpriteData:
    ;; Y, #Tile, Attrs, X
    .db $78,$48,$00,80
    .db $78,$45,$00,87
    .db $78,$48,$00,94
    .db $78,$48,$00,108
    .db $78,$45,$00,115
    .db $78,$48,$00,122
    .db $90,$00,$00,80
    .db $90,$02,$00,90
    .db $90,$03,$00,100
    .db $90,$04,$00,110

    .org $fffa
    .dw nmi                     ; Handler for NMI (VBlank is starting)
    .dw init                    ; Handler for reset
    .dw 0

    .bank 2
    .org $0000
    .incbin "asciipong.chr"
