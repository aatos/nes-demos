all:
	make -C pong
	make -C helloworld

.PHONY: clean

clean:
	make -C pong clean
	make -C helloworld clean
