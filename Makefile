.PHONY: all
all:
	make -C pong
	make -C helloworld
	make -C simpler-helloworld

.PHONY: clean
clean:
	rm -f *.chr
	make -C pong clean
	make -C helloworld clean
	make -C simpler-helloworld clean
