# Requires bmp2chr to transform bitmap to format understood by NES (found from
# http://bobrost.com/nes/files/nes_sprite_tools_2004_03_18.zip) and nesasm
# (version 2 found from http://bobrost.com/nes/files/nbasic_2004_03_14.zip and
# version 3 http://www.nespowerpak.com/nesasm/nesasmsrc.zip)

%.chr: %.bmp
	bmp2chr $< $@

%.nes: %.asm
	nesasm3 $<

.PHONY: clean
clean:
	rm -f *.chr *.nes
